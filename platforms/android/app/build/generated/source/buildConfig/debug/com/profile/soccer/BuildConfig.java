/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.profile.soccer;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.profile.soccer";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 20000;
  public static final String VERSION_NAME = "2.0.0";
}
